package pl.plusgps.zadanierekrutacyjne;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class FibonacciAdapter extends RecyclerView.Adapter<FibonacciAdapter.ViewHolder> {

  private final List<String> mDataset;

  public List<String> getDataset() {
    return mDataset;
  }

  public FibonacciAdapter(List<String> myDataset) {
    mDataset = myDataset;
  }

  @Override public FibonacciAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
    return new ViewHolder(v);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    holder.mTextView.setText(mDataset.get(position));
  }

  @Override public int getItemCount() {
    return mDataset.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    public TextView mTextView;

    public ViewHolder(View v) {
      super(v);
      mTextView = (TextView) v.findViewById(R.id.text);
    }
  }
}