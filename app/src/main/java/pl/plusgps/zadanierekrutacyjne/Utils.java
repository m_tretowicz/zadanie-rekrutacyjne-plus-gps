package pl.plusgps.zadanierekrutacyjne;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import java.math.BigInteger;

public class Utils {

  public static BigInteger fibonacciLoop(int maxNumber) {
    if (maxNumber == 0 || maxNumber == 1) {
      return BigInteger.ONE;
    } else {
      BigInteger f0 = BigInteger.ONE;
      BigInteger f1 = BigInteger.ONE;
      for (int i = 2; i <= maxNumber; i++) {
        BigInteger fi = f0.add(f1);
        f0 = f1;
        f1 = fi;
      }
      return f1;
    }
  }

  public static void hideKeyboard(Activity activity) {
    // Check if no view has focus:
    View view = activity.getCurrentFocus();
    if (view != null) {
      InputMethodManager inputManager =
          (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
      inputManager.hideSoftInputFromWindow(view.getWindowToken(),
          InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }
}
