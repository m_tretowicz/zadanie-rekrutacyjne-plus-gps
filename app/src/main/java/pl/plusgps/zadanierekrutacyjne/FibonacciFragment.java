package pl.plusgps.zadanierekrutacyjne;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class FibonacciFragment extends Fragment {

  //Rekrutacja ­ Android developer ­ część 2
  //Czas na rozwiązanie: 60­180 minut
  //● Calculate the and display each Fibonacci number from 1 ­> max N possible on
  //an Android phone with unsigned integers, and display each F(n) in a table view.
  //The scrolling MUST remain smooth

  @InjectView(R.id.fibonacci_input) EditText fibonacciInput;
  @InjectView(R.id.my_recycler_view) RecyclerView mRecyclerView;

  public FibonacciFragment() {
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    final View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.inject(this, view);
    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mRecyclerView.setHasFixedSize(true);
    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
    mRecyclerView.setLayoutManager(mLayoutManager);

    List<String> dataset = new ArrayList<>();
    dataset.add(Utils.fibonacciLoop(1) + "");

    RecyclerView.Adapter mAdapter = new FibonacciAdapter(dataset);
    mRecyclerView.setAdapter(mAdapter);
  }

  @Override public void onDestroyView() {
    ButterKnife.reset(this);
    super.onDestroyView();
  }

  @SuppressWarnings("unused")
  @OnTextChanged(value = R.id.fibonacci_input, callback = OnTextChanged.Callback.TEXT_CHANGED)
  void onBeforeTextChanged(CharSequence text) {
    if (StringUtils.startsWith(text + "", "-")) {
      fibonacciInput.setText(StringUtils.stripStart(text + "", "-"));
    }
  }

  @SuppressWarnings("unused") @OnClick(R.id.ok_button) void okClicked() {

    Utils.hideKeyboard(getActivity());
    
    final Editable text = fibonacciInput.getText();
    if (StringUtils.isEmpty(text + "")) {
      Toast.makeText(getActivity(), R.string.empty_fibonacci_input_alert, Toast.LENGTH_SHORT)
          .show();
      return;
    }
    int max = Integer.valueOf(text + "");

    if (max > 500) {
      // dla większych liczb można zastosować np. intentservice, ale do zadania nie widzę takiej potrzeby
      Toast.makeText(getActivity(), R.string.too_big_fibonacci_alert, Toast.LENGTH_SHORT).show();
      return;
    }

    FibonacciAdapter adapter = (FibonacciAdapter) mRecyclerView.getAdapter();
    adapter.getDataset().clear();
    adapter.notifyDataSetChanged();

    List<String> dataset = new ArrayList<>();
    for (int i = 1; i <= max; i++) {
      dataset.add(Utils.fibonacciLoop(i) + "");
    }
    adapter.getDataset().addAll(dataset);
    adapter.notifyDataSetChanged();
  }
}
